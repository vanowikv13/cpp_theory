// Day 7 - pointer to pointer.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

int alloc_foo(int** foo)
{
	*foo = new int[10];
	return 1;
}

int** array_of_pointers(int x) {
	int** pointers_array = new int* [x];
	return pointers_array;
}


int main()
{
	int* ptr;
	alloc_foo(&ptr);
	ptr[9] = 9;
	std::cout << ptr[9] << "\n";

	auto ptr_arr = array_of_pointers(5);
	//ptr_arr[5] = new int[1];
	auto t = ptr_arr[5];
	t = new int[2];
	t[0] = 12;
	std::cout << t[0] << "\n";

	delete t;
	delete ptr_arr;
}