# Pointer to pointer

### What is it?

Pointer that keep address of another pointer, but it's also a dereference of a dereference (in a statement). It's is used often in C cause they doesn't have & notation for references. To update a return values which is a pointer.

### When to use it?

1) Mostly if you really need to operate on a pointers in function an not only on it value.
2) Array of pointers
3) To create dinamicly alocated pointers (no point)